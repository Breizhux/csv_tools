#!/usr/bin/env python3

from setuptools import setup, find_packages

setup(
    name = 'csv_tools',
    version = '1.0',
    packages = find_packages(
        exclude = [
            "csv_tools_without_obj.py"
        ]
    ),
    author = "Breizhux",
    author_email = "xavier.lanne@mailoo.org",
    description = "CSV Tools is a set of tools to read, write and manage csv files in python.",
    install_requires = [
        "statistics",
        "fuzzywuzzy"
    ],
    #include_package_data=True,
    url = 'https://gitlab.com/Breizhux/csv_tools',
    #classifiers=[
    #    "Programming Language :: Python",
    #    "License :: Th Unlicense",
    #    "Natural Language :: French",
    #    "Operating System :: Linux",
    #    "Programming Language :: Python :: 3.9",
    #    "Topic :: Tools",
    #],
)
