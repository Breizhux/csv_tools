#!/usr/bin/python3
# -*- coding : utf-8 -*-

from statistics import mean
from fuzzywuzzy import process

def extract_values(line, coma=',', field='"') :
    """ Read a line, and return the line split
    by values """
    values = []
    lock = False
    value = ""
    for char in line[:-1] :
        if char == field :
            lock = False if lock else True
        elif char == coma and not lock :
            values.append(value)
            value = ""
        else :
            value += char
    values.append(value)
    return values

def iter_csv(infile, **kwargs) :
    """ Read a csv by iteration """
    with open(infile, 'r') as file :
        for line in file.readlines() :
            yield extract_values(line, **kwargs)

def read_csv(infile, **kwargs) :
    """ Read a csv file. Return a list of line
    of values"""
    result = []
    for line in iter_csv(infile, **kwargs) :
        result.append(line)
    return result

def get_columns_names(infile,**kwargs) :
    """ Return juste the first line"""
    for i in iter_csv(infile,**kwargs) :
        return i

def get_columns_index(infile, column_name=[], **kwargs) :
    """ Return the index of columns required or
    a dict for all column"""
    column = get_columns_names(infile)
    if len(column_name) > 0 :
        index = []
        for i in column_name : index.append(column.index(i))
        return index
    index_column = {}
    for i, j in enumerate(column) : index_column[j] = i
    return index_column

def write_csv(file, values, coma=',', field='"') :
    """ Write a line in a csv file """
    line = []
    for value in values :
        if coma in str(value) :
            line.append("\"{}\"".format(str(value).replace('\n', '')))
        else :
            line.append(str(value).replace('\n', ''))
    file.write(",".join(line)+"\n")

def find_diff(file1, file2, show_all=False, **kwargs) :
    value_diff = False
    field_diff = False
    first = read_csv(file1, **kwargs)
    second = read_csv(file2, **kwargs)
    if len(first) != len(second) :
        print("Les deux fichiers n'ont pas le même nombre de ligne...")
        return 1
    for l in range(len(first)) :
        display_lines = False
        line1 = first[l]
        line2 = second[l]
        if len(line1) < len(line2) :
            print("fichier1, ligne{} : champ de valeur manquante".format(l+1, len(line1), len(line2)))
            if show_all : display_lines = True
            value_diff = True
        elif len(line2) < len(line1) :
            print("fichier2, ligne{} : champ de valeur manquante ({}/{})".format(l+1, len(line2), len(line1)))
            if show_all : display_lines = True
            value_diff = True
        else :
            for c in range(len(line1)) :
                v1 = line1[c]
                v2 = line2[c]
                if v1 != v2 :
                    field_diff = True
                    print("ligne{}, colonne{} : \"{}\"→\"{}\"".format(l+1, c+1, v1, v2))
                    if show_all : display_lines = True
        if display_lines :
            print("  f1,l{}: {}".format(l+1, str(line1)[1:-1]))
            print("  f2,l{}: {}".format(l+1, str(line2)[1:-1]))
            print("--------")

    if value_diff : return 2
    elif field_diff : return 2
    return 0

def simple_merge(sources, output, *args, **kwargs) :
    """ Merge csv files simply by columns."""
    with open(output, 'w') as file :
        lines = {}
        smalest_file = 0
        for filename in sources :
            lines[filename] = read_csv(filename)
            size = len(lines[filename])
            if size < smalest_file or smalest_file == 0 :
                smalest_file = size
        for i in range(smalest_file) :
            values = []
            for filename in lines :
                values += lines[filename][i]
            write_csv(file, values)

def smart_search(base_string, filename, columns_index, minimum_score=74) :
    """ Compares a string with the lines of a file according to
    the indicated columns. Returns the most appropriate line.
    smart_search("Jean Dupont", "inputfile.csv", ["Firstname", "Lastname"])
    return the complete line with best match, and the score result.
    """
    #Get the list of strings to compare
    compared_strings = {} #string : [complete_line]
    for i, line in enumerate(iter_csv(filename)) :
        if i == 0 : continue
        chaine = " ".join([line[c] for c in columns_index]).lower()
        compared_strings[chaine] = line
    #Get the best match
    best_match = process.extractOne(base_string, compared_strings.keys())
    if best_match[1] >= minimum_score :
        return (compared_strings[best_match[0]], best_match[1])
    else :
        return ([""]*len(line), 0.01)

def smart_merge(basefile, sources, compare_columns, outfile, column_order,
                                                factor_ocscore=4.0, **kwargs) :
    """ Merge many csv files in one file not by columns, but by smart data
    matching.
    factor_ocscore : if best_occurence / occurence of best_score > factor_ocscore
        return best occurence line (else best score line)
    """
    write_csv(outfile, column_order)
    #column index by filename
    columns_index_file = {}
    for f in sources :
        columns_index = {}
        for i, j in enumerate(get_columns_names(f)) :
            columns_index[j] = i
        columns_index_file[f] = columns_index
    base_columns = columns_index_file[basefile]
    # replace column name by index
    indexed_sources = {}
    for file in sources :
        index = []
        for column in sources[file] :
            index.append(columns_index_file[file][column])
        indexed_sources[file] = index
    c = []
    for comparaison in compare_columns :
        comp = {}
        for file in comparaison :
            index = []
            for column in comparaison[file] :
                index.append(columns_index_file[file][column])
            comp[file] = index
        c.append(comp)
    compare_columns = c
    del c, index, column, comp
    # MAINLOOP FOR GET RESULT OF COMPARAISON
    for a, baseline in enumerate(iter_csv(basefile)) :
        if a == 0 : continue
        #Get the best match lines for files for all comparaison conditions
        result_by_file = {}.fromkeys(sources.keys()) #{file : [(best_line, score), ],}
        result_by_file.pop(basefile)
        nbr_comp = 0
        for comparaison in compare_columns :
            #Get the compared string
            compared_string = " ".join([baseline[i] for i in comparaison[basefile]]).lower()
            if len(compared_string.strip(" ")) == 0 : continue
            # Get the best matchs for all files by column
            for comparing_file in comparaison :
                if comparing_file == basefile : continue
                best_match = smart_search(compared_string, comparing_file, comparaison[comparing_file], **kwargs)
                if result_by_file[comparing_file] is None :
                    result_by_file[comparing_file] = [best_match]
                else :
                    result_by_file[comparing_file].append(best_match)
        #Get the occurence of lines :
        merged_partial_results = {} #{file : [(line, occurence, score_average)]}
        for file in result_by_file :
            result_by_lines = [] #(line, occ, [scores])
            for result in result_by_file[file] :
                r_line = result[0]
                r_score = result[1]
                data = ""
                for i in result_by_lines :
                    if i[0] == r_line : data = i
                if data != "" :
                    occurence = data[1] + 1
                    score = [data[1]]
                    score.append(r_score)
                    result_by_lines.remove(data)
                    result_by_lines.append((r_line, occurence, score))
                else :
                    result_by_lines.append((r_line, 1, [r_score]))
            #change list of scores in to average
            for result in result_by_lines :
                if file in merged_partial_results :
                    merged_partial_results[file].append((result[0], result[1], mean(result[2])/100))
                else :
                    merged_partial_results[file] = [(result[0], result[1], mean(result[2])/100)]
        #clean python
        del result_by_lines, result, result_by_file
        #Get THE results by filename
        the_result = {basefile : baseline} #{file : line, file : line}
        for file in merged_partial_results :
            best_occurence = ("", 0.01, 0.01) #(line, occurence, score)
            best_score = ("", 0.01, 0.01) #(line, occurence, score)
            for result in merged_partial_results[file] :
                if result[1] > best_occurence[1] :
                    best_occurence = result
                elif result[2] > best_score[2] :
                    best_score = result
                    if result[2] == 1 : break
            if best_score[2] == 1 :
                the_result[file] = best_score[0]
            elif best_occurence[0] == best_score[0] :
                the_result[file] = best_score[0]
            elif (best_occurence[1]/best_score[1]) >= factor_ocscore :
                the_result[file] = best_occurence[0]
            else :
                the_result[file] = best_score[0]
        #Get and write the result in output file
        values_by_columns = {} #{column name : values}
        for file in sources :
            for column in sources[file] :
                index = columns_index_file[file][column]
                values_by_columns[column] = the_result[file][index]
        values = [] #column in good order
        for column in column_order :
            values.append(values_by_columns[column])
        write_csv(outfile, values)

if __name__ == "__main__" :
    #TEST READ
    #print("Test read")
    #path = "test_data.csv"
    #lines = read_csv(path)
    #[print(i) for i in lines]

    #TEST ITER
    #print("Test iteration")
    #path = "test_data.csv"
    #[print(i) for i in iter_csv(path)]

    #TEST WRITE
    #print("Test write")
    #out = "output_write.csv"
    #data = [["id","first_name","last_name","email","ip_address"],
    #        ["1","Ezra","Dux,barry","eduxbarry0@aboutads.info","85.165.190.23"]]
    #with open(out, 'w') as file :
    #    for values in data :
    #        write_csv(file, values)

    #TEST DIFF
    print("Test diff")
    file1 = "test_data1.csv"
    file2 = "test_data2.csv"
    find_diff(file1, file2, show_all=True)

    import sys
    sys.exit()

    #TEST SIMPLE MERGE :
    source = ["test_data1.csv", "test_data2.csv"]
    simple_merge(source, "output_simple_merge.csv")

    #TEST SMART SEARCH :
    print("Test smart search")
    matched_line = smart_search("Viva Reder", #find string
                                "test_data1.csv", #in file
                                get_columns_index("test_data1.csv", ["first_name", "last_name"]) # with this column number
                                )
    print("\"{}\" found with score {}. Matched line is :\n{}".format("Viva Reder", matched_line[1], matched_line[0]))

    #TEST SMART MERGE (thanks do not kill your computer...)
    print("Test smart merge")
    #sources : {input_filename : [the column use for out file], }
    #you can have more than 2 files !
    sources = {
        "test_data1.csv" : ["first_name", "last_name", "email"],
        "test_data2.csv" : ["ip_address", "Field Name"]
    }
    #Condition for comparaison to found similarity between lines of files
    compare_columns = [
        {   #compare the column "mail" to "msExchMailboxGuid"
            "test_data1.csv" : ["email"],
            "test_data2.csv" : ["mail"]
        },
        {   #compare the columns "displayName" and "displayGn" to "cn"
            "test_data1.csv" : ["first_name", "last_name"],
            "test_data2.csv" : ["Prenom", "Nom"]
        },
    ]
    #The order of column for the out file
    column_order = ["first_name",
                    "last_name",
                    "email",
                    "Field Name",
                    "ip_address"]
    with open("output_smart_merge.csv", 'w') as file :
        smart_merge("test_data1.csv",   #the principal file use to construct the output
                    sources,
                    compare_columns,
                    file,               #object opened file
                    column_order,
                    #minimum_score=74,  #minimum score to mark line is matching
                    #factor_ocscore=4.0 #a ratio to make occurence most important as score
                    )
