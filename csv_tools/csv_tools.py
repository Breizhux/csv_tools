#!/usr/bin/python3
# -*- coding : utf-8 -*-

import os
from statistics import mean
from fuzzywuzzy import process

class CSVFile :
    """ Manage the csv file in object."""
    def __init__(self, path, coma=',', field='"') :
        self.path = path
        self._coma = coma
        self._field = field

    def __repr__(self) :
        return "CSVFile<path=\"{}\", coma=\"{}\", field=\"{}\">".format(
            self.path, self._coma, self._field)

    def __iter__(self) :
        """ Read a csv by iteration """
        with open(self.path, 'r') as file :
            for i in file.readlines() :
                yield self._extract_values(i)

    def __len__(self) :
        """ Return the number of line."""
        lines_number = 0
        for i in self :
            lines_number += 1
        return lines_number

    def __getitem__(self, idx) :
        """ If int : return the ième line (None if index too big).
        If string : return the index of column name."""
        if isinstance(idx, str) :
            return self.get_columns_names().index(idx)
        elif not isinstance(idx, int) :
            return None
        for i, j in enumerate(self) :
            if i == idx :
                return j

    def __setitem__(self, index, values) :
        """ Write a line at specific index."""
        tmp_csv = CSVFile("{}.tmp".format(self.path),
                          coma=self._coma,
                          field=self._field)
        for i, l in enumerate(self) :
            if i != index :
                tmp.write(l)
            else :
                tmp.write(values)
        os.remove(self.path)
        os.rename(tmp_csv.path, self.path)

    def _extract_values(self, line) :
        """ Return a line split by values."""
        values = []
        lock = False
        value = ""
        for char in line[:-1] :
            if char == self._field :
                lock = False if lock else True
            elif char == self._coma and not lock :
                values.append(value)
                value = ""
            else :
                value += char
        values.append(value)
        return values

    def read_csv(self) :
        """ Return the list of line of values."""
        result = []
        for line in self :
            result.append(line)
        return result

    def write(self, values:list) :
        """ Write a line in a csv file."""
        line = []
        for value in values :
            if self._coma in str(value) :
                line.append("\"{}\"".format(str(value).replace('\n', '')))
            else :
                line.append(str(value).replace('\n', ''))
        with open(self.path, 'a') as file :
            file.write(",".join(line)+"\n")

    def write_lines(self, lines:list) :
        """ Write a some lines in csv. Required a list of list."""
        for i in lines : self.write(i)

    def get_columns_names(self) :
        """ Return juste the first line."""
        for i in self : return i

    def get_columns_index(self, column_name=[], **kwargs) :
        """ Return the index of columns required in dict :
        {column_name : column_index}. If no specific name
        of column set, return index for all column."""
        column = self.get_columns_names()
        index = {}
        if len(column_name) > 0 :
            for i in column_name : index[i] = column.index(i)
            return index
        for i, j in enumerate(column) : index[j] = i
        return index


FIND_DIFF_MESSAGE = [
    "Les deux fichiers n'ont pas le même nombre de ligne...",
    "fichier 1, ligne {} : champ de valeur manquante ({}/{})",
    "fichier 2, ligne {} : champ de valeur manquante ({}/{})",
    "ligne{}, colonne{} : \"{}\"→\"{}\"",
    "  f1,l{}: {}",
    "  f2,l{}: {}",
]
def find_diff(file1, file2, show_all=False, **kwargs) :
    value_diff = False
    field_diff = False
    first = CSVFile(file1, **kwargs)
    second = CSVFile(file2, **kwargs)
    if len(first) != len(second) :
        print(FIND_DIFF_MESSAGE[0])
        return 1
    ln = 0 #line_number
    for line1, line2 in zip(first, second) :
        ln += 1
        display_lines = False
        if len(line1) < len(line2) :
            print(FIND_DIFF_MESSAGE[1].format(ln, len(line1), len(line2)))
            if show_all : display_lines = True
            value_diff = True
        elif len(line2) < len(line1) :
            print(FIND_DIFF_MESSAGE[2].format(ln, len(line2), len(line1)))
            if show_all : display_lines = True
            value_diff = True
        else :
            cn = 0 #column_number
            for v1, v2 in zip(line1, line2) :
                cn += 1
                if v1 != v2 :
                    field_diff = True
                    print(FIND_DIFF_MESSAGE[3].format(ln, cn, v1, v2))
                    if show_all : display_lines = True
        if display_lines :
            print(FIND_DIFF_MESSAGE[4].format(ln, str(line1)[1:-1]))
            print(FIND_DIFF_MESSAGE[5].format(ln, str(line2)[1:-1]))
            print("--------")

    if value_diff : return 2
    elif field_diff : return 2
    return 0

def simple_merge(sources, output, *args, **kwargs) :
    """ Merge csv files simply by columns."""
    out_csv = CSVFile(output)
    sources = [CSVFile(i) for i in sources]
    for i in zip(*sources) :
        values = []
        for j in i : values += j
        out_csv.write(values)

def smart_search(string, csv, columns, minimum_score=74,
                            algo=process.default_scorer, **kwargs) :
    """ Compares a string with the lines of a file according to
    the indicated columns. Returns the most appropriate line.
    smart_search("Jean Dupont", "inputfile.csv", ["Firstname", "Lastname"])
    return the complete line with best match, and the score result.
    """
    if isinstance(csv, str) : csv = CSVFile(csv)
    columns_index = csv.get_columns_index(column_name=columns)
    accepted_scores = [minimum_score, None, None] #score, index, line
    for i, l in enumerate(csv) :
        if i == 0 : continue
        chain = " ".join([l[c] for c in columns_index.values()]).lower()
        score = algo(string, chain)
        if score >= accepted_scores[0] :
            accepted_scores = [score, i, l]
    return accepted_scores #score, index, line

def smart_line_finder(master:tuple, slave, rules:dict, **kwargs) :
    """ Find a line with different rules to compare at multiple level.
    master : [line], {complete column index}
    slave : "path_to_file.csv" or CSVFile("path_to_file.csv")
    rules : [{
            "compare_to" : [nom des colonnes de main pour comparaison],
            "compare_from" : [nom des colonnes de ce fichier pour comparaison],
            "algo" : fonction de comparaison. Si None : default
        }, {
            "compare_to" : [nom des colonnes de main pour comparaison],
            "compare_from" : [nom des colonnes de ce fichier pour comparaison],
            "algo" : fonction de comparaison. Si None : default
        }
    ]
    """
    #convert path to CSVFile object
    if isinstance(slave, str) : slave = CSVFile(slave)
    #get bests matchs lines
    matchs = {} #index : (score, occurence, line)
    for rule in rules :
        #get index and string of src column for comparison.
        compared_index = [master[1][i] for i in rule["compared"]]
        compared_string = " ".join([master[0][c] for c in compared_index]).lower()
        #set option for smart_search funtion
        options = kwargs
        for i in rule :
            if not i in ["compared", "comparing"] : options[i] = rule[i]
        #get result from smart_search function and save it on matchs dict
        result = smart_search(compared_string, slave, rule["comparing"], **options)#score, index, line
        if result[1] in matchs :
            matchs[result[1]] = (matchs[result[1]][0]+result[0],
                                 matchs[result[1]][1]+1,
                                 result[2])
        else :
            matchs[result[1]] = (result[0], 1, result[2])
    #extract best true match by compute moyenne
    best_match = (0, None, None) #fct_ocscore, index, line
    for i in matchs :
        factor_ocscore = matchs[i][0]/matchs[i][1]
        if factor_ocscore > best_match[0] :
            best_match = (factor_ocscore, i, matchs[i][2])
    #print("matchs :\n{}".format(best_match))
    return best_match #fct_ocscore, index, line

def smart_merge(outfile, master, slaves, outcolumn, **kwargs) :
    """ Intelligent merge csv files by comparing certain columns of csv.
    Create and output file. Argument are :
    - outfile : the file to write the merge result.
        ex: "path/to/out.csv" or CSVFile("path/to/out.csv")
    - masterfile : the master file to follow to create out file and its keep columns.
        ex: ("path/to/file.csv" or CSVFile("path/to/file.csv"), [list of column name to keep])
    - slavesfiles : a list of dict for slave file with multiple parameters :
        - file name : the file path.
            ex: "filename" : "path/to.csv" or CSVFile("path/to.csv")
        - out : list of column to keep on end file.
            ex: "out" : [list of column to keep]
        - rules : the rule to compare master file with column name :
            - compared : the columns name of master file to use to compare at slave file.
                ex: "compared" : [list of column name]
            - comparing : the columns name of slave file to use to compare at master file.
                ex: "comparing" : [list of column name]
            - algorithm (optionnal): algorithm use to compute the levenstein distance.
                ex: "algo" : process.extractOne
    - outcolumn : list of tuple to indicate the order of keep column by filename.
        ex: [(filename, column name to keep), ]
    """
    #If file path is not a CSV object, create the object.
    if os.path.exists(outfile) : os.remove(outfile)
    if isinstance(outfile, str) : outfile = CSVFile(outfile)
    if isinstance(master[0], str) : master = (CSVFile(master[0]), master[1])
    for i in slaves :
        if isinstance(i["file"], str) : i["file"] = CSVFile(i["file"])
    #write the column name on outfile
    outfile.write([i[1] for i in outcolumn], **kwargs)
    #keep index of column from master file
    master_index = master[0].get_columns_index()
    #loop on master file to compare each line by using rules
    for i, master_line in enumerate(master[0]) :
        if i == 0 : continue
        #print("\nmaster line : {}".format(master_line))
        #the future line to write in result
        result_line = outcolumn.copy() #(file, column name) → replace by matched element
        #replace line by matched element for master file
        for j, e in enumerate(result_line) :
            if isinstance(e, tuple) and e[0] == master[0].path :
                result_line[j] = master_line[master_index[e[1]]]
        #for each slave file get the best line match
        for file in slaves :
            #configure option
            options = kwargs
            for i in file :
                if not i in ["file", "out", "rules"] : options[i] = file[i]
            #get index and best line to match with slave
            slave_index = file["file"].get_columns_index()
            slave_line = smart_line_finder((master_line, master_index),
                                           file["file"],
                                           file["rules"],
                                           **kwargs)[2]
            #replace element on result line by good element of matched line
            for j, e in enumerate(result_line) :
                if isinstance(e, tuple) and e[0] == file["file"].path :
                    result_line[j] = slave_line[slave_index[e[1]]]
        #print("resulted line :\n{}".format(",".join(result_line)))
        outfile.write(result_line, **kwargs)


#if __name__ == "__main__" :
    #import sys
    #TEST READ
#    print("Test read")
#    csv = CSVFile("data_tests.csv")
#    lines = csv.read_csv()
#    [print(i) for i in lines]
#    del lines, csv


    #TEST ITER
#    print("Test iteration")
#    csv = CSVFile("data_tests.csv")
#    [print(i) for i in csv]
#    del csv


    #TEST WRITE
#    print("Test write")
#    csv = CSVFile("output_write.csv")
#    data = [["id","first_name","last_name","email","ip_address"],
#            ["1","Ezra","Duxarry","eduxbarry0@aboutads.info","85,165,190.23"]]
#    for values in data :
#        csv.write(values)


    #TEST WRITE_LINES
#    print("Test write_lines")
#    csv = CSVFile("output_write.csv")
#    lines = [["id","first_name","last_name","email","ip_address"],
#            ["1","Ezra","Duxarry","eduxbarry0@aboutads.info","85,165,190.23"]]
#    csv.write_lines(lines)


    #TEST DIFF
#    print("Test diff")
#    file1 = "test_data1.csv"
#    file2 = "test_data2.csv"
#    find_diff(file1, file2, show_all=True)


    #TEST SIMPLE MERGE :
#    source = ["test_data1.csv", "test_data2.csv"]
#    simple_merge(source, "output_simple_merge.csv")


    #TEST SMART SEARCH :
#    print("Test smart search")
#    finded_str = "Viva Reder"
#    line, index, score = smart_search(finded_str,                  #finded string
#                                      "test_data1.csv",            #in file
#                                      ["first_name", "last_name"]  # with this column number
#                                     )
#    print("\"{}\" found with score {} in line :\n{}".format(finded_str, score, line))


    #TEST SMART LINE FINDER
#    print("Test smart line finder with multiple rules")
#    master = (
#        ["1", "Ezra", "Dux,barry", "eduxbarry0@aboutads.info"], CSVFile("test_data0.csv").get_columns_index()
#    )
#    slave = "test_data1.csv"
#    rules = [{
#        "compared" : ["first_name"],
#        "comparing" : ["first_name"],
#        #"algo (optionnel)" : fonction de comparaison. Si None : default
#        }, {
#        "compared" : ["last_name", "mail"],
#        "comparing" : ["last_name", "email"],
#        #"algo (optionnel)" : fonction de comparaison. Si None : default
#        }
#    ]
#    result = smart_line_finder(master, slave, rules)
#    print("Result found :\n  - Original line : {}\n  - Found line : {}\n  - Score : {}".format(
#           master[0], result[2], result[0]))


    #TEST SMART MERGE (thanks do not kill your computer...)
#    print("Test smart merge")

    #masterfile corresponding of the followed file to build the merged file.
    #Is a tuple : (the file path, [the list of keep column])
#    masterfile = ("test_data0.csv", ["id", "last_name"])

    #slavesfiles corresponding of the other csv used to add information in merged file
    #Is a list of dictionnary. 1 dictionnary per csv file.
#    slavesfiles = [{
            #the file path, or a CSVFile object
#            "file" : "test_data1.csv",
            #the column to keep in output
#            "out" : ["firstname", "email"],
            #the rules to use to create correspondance between her and master.
#            "rules" : [#configure a list of rules in dict
#                {#compare columns "first_name" and "last_name" of master with columns "firstname" and "lastname" of slave
#                    "compared" : ["first_name", "last_name"],
#                    "comparing" : ["firstname", "lastname"],
                    #you can configure the fuzzywuzzy algorithme to use for this rule
                    #"algo" : None
#                },
#                {
#                    "compared" : ["id"],
#                    "comparing" : ["id"],
#                    #"algo" : None
#                }
#            ]
#            #"algo" : None #you can configure the fuzzywuzzy algorithme to use for all rule of this slave (not priority on specific rule)
#        }, {
#            "file" : "test_data2.csv",
#            "out" : ["ip_address", "Field Name"],
#            "rules" : [
#                {#one rule here to compare slave and master with same column name : "mail"
#                    "compared" : ["mail"],
#                    "comparing" : ["mail"],
#                    #"algo" : None
#                }
#            ]
#        }
#    ]

    #The order of output column
#    out_column_order = [
        #The corresponding file, the column name
#        ("test_data0.csv", "id"),
#        ("test_data1.csv", "firstname"),
#        ("test_data0.csv", "last_name"),
#        ("test_data1.csv", "email"),
#        ("test_data2.csv", "ip_address"),
#        ("test_data2.csv", "Field Name")
#    ]

    #The call of merged function. First arg is the output file
#    smart_merge("merged_file.csv", masterfile, slavesfiles, out_column_order)
