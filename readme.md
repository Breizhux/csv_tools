# CSV-TOOLS for python

A simple tools to work on csv.

The csv library of python is not simple and have many error to read csv when field contain a coma : it just split lines, and no find to escape coma when is in text value...

Add some function to compute csv.

## Installation

Easily from pip
```
pip install git+https://gitlab.com/Breizhux/csv_tools.git
```

## Usage

### Open CSV :

```
import csv_tools
file_path = "path/to/input.csv"

my_csv = csv_tools.CSVFile(file_path)

#You can give additional parameters to indicate the separation character or text fields :
my_csv = csv_tools.CSVFile(file_path, coma=';', field="'")
```

### Read CSV :
```
#Get all lines in a list of values
lines = my_csv.read_csv()

#You can also iterate the file :
for values in my_csv :
    print(values)

#You can get the number of line in csv :
print(len(my_csv))

#You can get directly a specific line :
line4 = my_csv[3] #line 1 is index 0

#You can get the column name (first line) :
my_csv.get_columns_names()
#Or get the column name and here index :
my_csv.get_columns_index()
```

### Write CSV :
```
#write a line
my_csv.write(["my", "list", "of", "values"])

#or write some of lines
my_csv.write_lines([["first", "line"], ["second", "line"], ["etc", "..."]])

#You can write line directly at an index
my_csv[4] = ["my", "list", "of", "values"] #write values at line 4
```

### Find differences between two csv files :
Simply show the difference between two files.
```
import csv_tools
path1 = "test_data0.csv"
path2 = "test_data1.csv"
csv_tools.find_diff(path1, path2, show_all=True) #show_all=False by default : if True, show line when there is difference.
```

### Simple merge for many CSV files :
Simply write line by line many csv in one csv.
```
import csv_tools
list_of_files_to_merge = ["test_data1.csv", "test_data2.csv"]
csv_tools.simple_merge(list_of_files_to_merge, "output_simple_merge.csv")
```

### Find an aproximative element in a columns of a file :
Search for a string in a csv with the columns in which the string could match. Use partial correspondance.
```
import csv_tools
#find "Viva Reder" in column "first_name" and "last_name" of "test_data1.csv" file.
line, index, score = csv_tools.smart_search("Viva Reder",
                                  "test_data1.csv",
                                  ["first_name", "last_name"]
                                 )
print("Best matched line are found with score {} is : {}".format(score, line))

```
Optionnal options :
 - minimum_score (default=74) : the minimum score to reach.
 - compute_algo (default=process.default_scorer) : the algorithm to use to get the corresponding string.


### Find a line in another CSV with multiple rules
This function will found the closest line to the source line with multiple comparison.

**master** : The master row to compare to the rows in the csv file. Need the complete line and the column index.

**slave** : The slave file path or CSVFile object.

**rules** : A dict contain all rules used to determine the best corresponding line in slave file to the master line.
 - *compared* : list of column name of master used for comparison.
 - *comparing* : list of column name of slave used for comparison.
 - *algo* (optionnal) : algorithme to used for this comparison rule (see fuzzywuzzy module).
 - *minimum_score* (optionnal) : the minimum of score to considered matching.

**optionnal** : Does not take precedence over specific rules.
 - *algo* : algorithme to used for all comparison rules.
 - *minimum_score* : the minimum score to considered matching.

```
import csv_tools
master = (
    #the totality of line
    ["1", "Ezra", "Dux,barry", "eduxbarry0@aboutads.info"],
    #the column name and corresponding index
    CSVFile("test_data0.csv").get_columns_index()
)

slave = "test_data1.csv"

rules = [{
        "compared" : ["first_name"],
        "comparing" : ["first_name"],
        #"algo (optionnel)" : fonction de comparaison. Si None : default
    }, {
        "compared" : ["last_name", "mail"],
        "comparing" : ["last_name", "email"],
    }
]

result = csv_tools.smart_line_finder(master, slave, rules)
print("Result found :")
print("  - Original line : {}".format(master[0]))
print("  - Found line : {}".format(result[2]))
print("  - Index of line : {}".format(result[1]))
print("  - Score : {}".format(result[0]))
```

### Merge multiple CSV by corresponding data of certains columns
Merge multiple file in one by matching the most similar lines together.

**master file** : You must to indicate the master file, and the column name used in the output file. The master file corresponding of the followed file to build the merged file.

**slaves files** : Is a list of other file to merge in master file. It contain the file path or csv object, the column keep to the merged file, the rules for comparison (see rules of `smart_line_finder` function).

**column_order** : the list of merged column name in the order as you want.

```
import csv_tools

#first, the csv path or CSVFile object. The the list of column to keep in merged file.
masterfile = ("test_data0.csv", ["id", "last_name"])

slavesfiles = [
    {
        #the file path, or a CSVFile object
        "file" : "test_data1.csv",
        #the column to keep in output
        "out" : ["firstname", "email"],
        #the list of rules
        "rules" : [
            {#compare columns "first_name" and "last_name" of master with columns "firstname" and "lastname" of slave
                "compared" : ["first_name", "last_name"],
                "comparing" : ["firstname", "lastname"],
                #"algo" is available for local rule
            },
            {
                "compared" : ["id"],
                "comparing" : ["id"],
             }
        ]
        #"algo" is available for all rules of file "test_data1.csv"

    }, {

        "file" : "test_data2.csv",
        "out" : ["ip_address", "Field Name"],
        "rules" : [
            {
                "compared" : ["mail"],
                "comparing" : ["mail"],
            }
        ]
    }
]

out_column_order = [
    #The corresponding file, the column name
    ("test_data0.csv", "id"),
    ("test_data1.csv", "firstname"),
    ("test_data0.csv", "last_name"),
    ("test_data1.csv", "email"),
    ("test_data2.csv", "ip_address"),
    ("test_data2.csv", "Field Name")
]

#Finally, do not forget to launch the merging of the files. First arg is the output file.
smart_merge("merged_file.csv", masterfile, slavesfiles, out_column_order)

#You can also pass arguments that will influence the rules. These parameters have the lowest priority compared to those configured in the rules.
#Ex:
smart_merge("merged_file.csv", masterfile, slavesfiles, out_column_order,
    algo=process.extractOne, minimum_score=50)
```
